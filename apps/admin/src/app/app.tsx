import * as React from 'react';
import styles from './app.module.css';

import MuiAppBar from './components/MuiAppBar';
import MuiTable from './components/MuiTable';
import MuiFormDialog from './components/MuiFormDialog';

export function App() {
  return (
    <React.Fragment>
      <MuiAppBar />
      <p />
      <MuiTable />
      <p />
      <MuiFormDialog />
    </React.Fragment>
  );
}

export default App;
